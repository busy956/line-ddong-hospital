import './App.css';
import Brown from "./components/Brown";

function App() {
  return (
    <div className="App">
      <Brown/>
    </div>
  );
}

export default App;
