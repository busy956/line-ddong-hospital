import React from "react";

const Brown = () => {
    // 여기가 몇 번 질문인지 알아야 한다 -> 어디서 끝나는지 정해줘야 하니까
    const [currentQuestion, setCurrentQuestion] = React.useState(0)
    // 처음의 점수와 현재의 점수를 세팅하는 것
    // 처음의 점수는 0점
    const [score, setScore] = React.useState(0)

    // 왜 만드는지 목적을 파악할 것
    // 질문을 해서 점수를 통해 문제가 있으니 병원에 가라고 하는 것
    // 목적을 적고 흐름을 파악하면 무슨 hook이 들어가는지 알 수 있다
    // 질문을 하고 점수를 매겨서 파악해야 한다
    const question = [
        {
            question: "최근 1 ~ 2일 사이에 대변을 보신 횟수는 어떤가요?",
            answer: [
                {text: "1 ~ 2회", score: 10},
                {text: "3 ~ 4회", score: 6},
                {text: "5회 이상", score: 2},
                {text: "0회", score: 0},
            ]
        },
        {
            question: "가장 최근에 보셨던 대변의 모양은 어떤가요?",
            answer: [
                {text: "바나나모양 똥", score: 10},
                {text: "약간 무른 똥", score: 6},
                {text: "금이 간 똥", score: 4},
                {text: "토끼 똥", score: 2},
                {text: "뭉쳐진 똥", score: 2},
                {text: "진흙 같은 똥", score: 2},
                {text: "설사", score: 0}
            ]
        },
        {
            question: "가장 최근에 보셨던 대변의 색깔은 어떤가요?",
            answer: [
                {text: "황갈색", score: 10},
                {text: "노란색", score: 2},
                {text: "초록색", score: 2},
                {text: "회백색", score: 2},
                {text: "붉은색", score: 0},
                {text: "검은색", score: 0}
            ]
        },
        {
            question: "가장 최근에 보셨던 대변의 냄새는 어떤가요?",
            answer: [
                {text: "평범한 냄새", score: 10},
                {text: "시큼한 냄새", score: 0},
                {text: "비린 냄새", score: 0},
                {text: "생선 썩는 냄새", score: 0}
            ]
        },
        {
            question: "가장 최근에 보셨던 대변의 굳기는 어떤가요?",
            answer: [
                {text: "평범해 보인다", score: 10},
                {text: "딱딱해 보인다", score: 4},
                {text: "무르게 보인다", score: 4},
                {text: "묽어 보인다", score: 2},
                {text: "물처럼 보인다", score: 0}
            ]
        },
        {
            question: "가장 최근에 보셨던 대변의 형태는 어떤가요?",
            answer: [
                {text: "평범해 보인다", score: 10},
                {text: "수분이 많아 보인다", score: 8},
                {text: "건조해 보인다", score: 4},
                {text: "갈라져 보인다", score: 2}
            ]
        },
        {
            question: "가장 최근에 보셨던 대변의 굵기는 어떤가요?",
            answer: [
                {text: "평범하다", score: 10},
                {text: "노란색", score: 8},
                {text: "초록색", score: 6},
                {text: "황갈색", score: 0}
            ]
        }
    ]

    // 결과로서 목업데이터 아래에 넣어둔다(흐름상 보기 편하게 할려고)
    // return을 string으로 준다
    // 점수를 받아서 결과를 준다
    const getResult = (score) => {
        // 만약에 점수가 60점 이상이면
        if (score >= 60) {
            return "당신은 건강해요"
            // 점수가 60점 미만이면
        } else {
            return "장에 문제가 있는 것 같아요. 병원에 가주세요"
        }
    }

        // 버튼을 눌렀을 때 무언가가 실행이 되야 한다
        // 매개변수 하나니까 괄호 생략가능
        const handleAnswerClick = answerScore => {
            setScore(score + answerScore)
            // 다음 질문은 현재 질문 + 1
            const nextQuestion = currentQuestion + 1
            // 만약에 다음 질문이 질문의 길이보다 작다면
            if (nextQuestion < question.length) {
                // 현재 질문에 다음번 질문을 넣는다
                setCurrentQuestion(nextQuestion)
            } else {
                // 질문이 끝나면 처음 점수에 대답 점수를 넣는다
                // 왜 다시 한번 더해주는 걸까?
                // 끝났는지 판단을 할 때 액션이 있어야 한다
                // 중간에 결과보기 버튼이 있었으면 괜찮은데 그게 아니라서 계산 시간을 안줬기 때문에 다시 더해주는 것
                alert(getResult(score + answerScore))
            }
        }

    // 코드의 본질이 프론트엔드니까 return에 디자인 + 비즈니스로직이 와야한다
    return (
        <div>
            <h2>{question[currentQuestion].question}</h2>
            {question[currentQuestion].answer.map((answer) => (
                <button key={answer.question} onClick={() => handleAnswerClick(answer.score)}>
                    {answer.text}
                </button>
            ))}
        </div>
    )
}

// 결과물은 하나로 보여야 한다
export default Brown

// 작업순서
// 컴포넌트 틀을 만든다
// export를 만든다 (어차피 빼는 거니까)
// return을 써준다 (프론트니까 결과물은 디자인 + 로직으로 나온다)]
// 목업데이터를 넣는다 (데이터를 넣는 것은 확정이니까)
// 목적을 파악하고 hook을 쓰는 것이 확정되면 hook을 써준다